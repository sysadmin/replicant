import os
import json
import time
import pygit2
from datetime import datetime

# Central class for managing individual repositories
class Repository(object):

	# Default metadata
	DefaultMetadata = {
		'id': None,
		'hash': '',
		'description': '',
		'default_branch': None,
		'deleted': False,
		'deleted_on': None,
		'path': '',
		'remote_url': '',
		'remote_last_update': 0,
		'visibility': '',
	}

	# Perform initial setup and get ready to go
	def __init__(self, manager, storage=None, metadata=None):
		# Prepare for use
		# We store the manager for later and initialise everything else for later use
		self.manager     = manager
		self.metadata    = dict()
		self.gitBackend  = None
		self.storagePath = None

		# First things first, we need to make sure we have either a storage path, or metadata
		# Without either we cannot go anywhere
		if type(storage) is not str and type(metadata) is not dict:
			raise Exception('Unable to locate repository - no storage path or metadata were provided')

		# Also, make sure we haven't been given both. If we have, then there be dragons so don't proceed
		if storage is not None and metadata is not None:
			raise Exception('Unable to access repository - both storage path and metadata were provided')

		# Now we can check to see if we were given a storage path
		# If we were, we should load the metadata from that
		if storage is not None and os.path.exists( storage ):
			# Preserve the storage path
			self.storagePath = storage

			# Load up the necessary attributes from 'replicant.json'
			metadataPath = os.path.join( self.storagePath, 'replicant.json' )
			metadataFile = open( metadataPath, 'r' )
			metadataJson = json.load( metadataFile )

			# Then start using it
			self.metadata = metadataJson

		# Next we check to see if we were given some metadata
		# If we were, then we need to get everything initialised as this is probably a brand new repository
		if metadata is not None:
			# Preserve the metadata we will need for later on
			self.updateMetadataFromGitlab( metadata )

			# Determine our storage path
			self.storagePath = os.path.join( manager.repositoriesPrefix, self.hash[0:2], self.hash[2:4], self.hash )

		# Everything else can be finalised once we are actually up and running
		return

	# Provide access to our metadata
	def __getattr__(self, attribute):
		# Do we have a value set for this?
		if attribute in self.metadata:
			return self.metadata[attribute]
		
		# Maybe there is a default value for it instead?
		if attribute in Repository.DefaultMetadata:
			return Repository.DefaultMetadata[ attribute ]

		# Otherwise we don't know
		return super().__getattr__(attribute)

	# Make it possible to change metadata values
	def __setattr__(self, attribute, value):
		# Is this a value which should be in our metadata?
		# If so, store it
		if attribute in Repository.DefaultMetadata:
			self.metadata[ attribute ] = value
			return value

		# Otherwise we don't know
		return super().__setattr__(attribute, value)

	# Convenience accessor for the PyGit2 instance used to manage the repository
	@property
	def backend( self ):
		# First, have we already performed the setup?
		if self.gitBackend is not None:
			return self.gitBackend

		# In that case, we need to set things up
		# First question, does our directory exist?
		if os.path.exists( self.storagePath ):
			# Then we can safely assume the repository has already been initialized
			self.gitBackend = pygit2.Repository( self.storagePath )
			return self.gitBackend

		# Otherwise, we need to perform the initial repository setup
		self.gitBackend = pygit2.init_repository( self.storagePath, bare=True, description=self.description, initial_head=self.default_branch )
		return self.gitBackend

	# Accessor for the last time the repository was modified
	# As Git does not make it possible to easily determine this, we use the last modified time of the objects/ directory
	@property
	def last_updated_at(self):
		# First make sure our Git repository actually exists
		# If it doesn't, we return 0 (1/1/1980)
		objectsDirPath = os.path.join( self.storagePath, 'objects' )
		if not os.path.exists( objectsDirPath ):
			return 0

		# Since it does exist, we can use the modification time of objects/
		return os.path.getmtime( objectsDirPath )

	# Update the metadata we have stored for this project
	def updateMetadataFromGitlab(self, newMetadata):
		# Grab the necessary bits and pieces we want from the Gitlab metadata
		self.id                 = newMetadata['id']
		self.description        = newMetadata['description']
		self.path               = newMetadata['path_with_namespace']
		self.remote_url         = newMetadata['http_url_to_repo']
		self.remote_last_update = datetime.strptime(newMetadata['last_activity_at'],  "%Y-%m-%dT%H:%M:%S.%fZ").timestamp()
		self.visibility         = newMetadata['visibility']

		# Sometimes repositories don't have a default_branch specified on the Gitlab side, so we have to be more careful grabbing it
		if 'default_branch' in newMetadata:
			self.default_branch = newMetadata['default_branch']

		# Sanity check our metadata
		# In particular, descriptions can be None from time to time
		if self.description is None:
			self.description = ''

		# Finally calculate our hash
		self.hash = self.manager.hashForRepository( self.id )

	# Write out our metadata for later use
	def save(self):
		# First check to see if the directory has been created yet
		# If we are a new repository, then the directory won't exist yet
		# We cannot proceed in this case, because we have to let Git create it (otherwise it will refuse to do so)
		if not os.path.exists( self.storagePath ):
			return False

		# Now that we know the directory exists, write out the metadata for future use
		metadataPath = os.path.join( self.storagePath, 'replicant.json' )
		with open(metadataPath, 'w', encoding='utf-8') as metadataFile:
			json.dump( self.metadata, metadataFile, ensure_ascii=False, indent=4 )

		# We'll do the same for the 'description' file used by CGit and other software
		descriptionPath = os.path.join( self.storagePath, 'description' )
		with open(descriptionPath, 'w', encoding='utf-8') as descriptionFile:
			descriptionFile.write( self.description )

		# All done
		return True

	# Update the repository we're mirroring from the remote server
	def refreshMirror(self, forceUpdate=False):
		# If the default branch has not been setup, chances are the repository has not yet been pushed
		# In that case, there is nothing to sync yet, so do nothing
		if self.default_branch is None:
			return False

		# First we need to make sure our mirror has been setup
		# Before we can do this though, we need a list of remotes in this repository
		ourRemoteName = 'upstream'
		knownRemotes  = [remote.name for remote in self.backend.remotes]

		# Check to see if our remote has been setup
		if ourRemoteName not in knownRemotes:
			# It doesn't seem to, so make it so
			self.backend.remotes.create('upstream', self.remote_url, "+refs/*:refs/*")
			# Make sure it has been configured as a mirror
			configName = "remote.{}.mirror".format( ourRemoteName )
			self.backend.config[ configName ] = True

		# In case the URL to the repository we mirror has changed, ensure it is up to date
		self.backend.remotes.set_url( ourRemoteName, self.remote_url )

		# Set ourselves up for authenticating to Gitlab when cloning
		authenticationCallback = pygit2.RemoteCallbacks(credentials=self.manager.repositoryCredentials)

		# Initiate the sync
		progress = self.backend.remotes[ ourRemoteName ].fetch( refspecs=["+refs/*:refs/*"], prune=pygit2.GIT_FETCH_PRUNE, callbacks=authenticationCallback )

		# Now we'll update HEAD if necessary to refer to the place it should refer to
		headReference = self.backend.lookup_reference("HEAD")
		headReference.set_target('refs/heads/' + self.default_branch)

		# To help ourselves later on, we should also set the last modified time of the repository to the same as when it was last modified on Gitlab
		# This is also needed to ensure CGit displays modified times correctly
		objectsDirPath = os.path.join( self.storagePath, 'objects' )
		os.utime( objectsDirPath, times=( time.time(), self.remote_last_update ), follow_symlinks=True )

		# Finally make sure our metadata is all flushed out to disk
		return self.save()
