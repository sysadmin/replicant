import gitlab
import Replicant

# This isn't the cleanest way to do things, but we don't have another way of passing state around unfortunately
repositoryManagerConfig = None
gitlabServer = None

# Updates a given project as requested
def updateProject( project_id ):
	# Setup our repository manager
	repositoryManager = Replicant.Manager( repositoryManagerConfig )

	# As the project may not yet be known by the repository manager, the first step is to ask Gitlab for details on this project
	gitlabProject = gitlabServer.projects.get( project_id )

	# Now that we have the details from Gitlab, we can safely ask the repository manager for this project
	project = repositoryManager.register( gitlabProject.attributes )

	# Make sure the metadata for the local mirror is up to date from Gitlab
	# Registration does not do this for existing projects - hence the need to force this here
	project.updateMetadataFromGitlab( gitlabProject.attributes )

	# Usually we would do checks to see if the repository had changed vs. what we have stored locally
	# As this only runs when Gitlab informs us of some form of change, we can safely assume it needs to be synced
	project.refreshMirror()

	# Because the change could have included a name change, we should also rebuild the local symlink tree
	repositoryManager.updateExportedTree()

	# All done!
	return True

# Marks a project as deleted as requested
def deleteProject( project_id ):
	# Setup our repository manager
	repositoryManager = Replicant.Manager( repositoryManagerConfig )

	# Ask the repository manager for this project
	project = repositoryManager.retrieve( project_id )
	
	# Mark the project as deleted
	repositoryManager.markAsDeleted( project )
	
	# Finally, rebuild the local symlink tree to remove it from view
	repositoryManager.updateExportedTree()
	
	# All done!
	return True

# Goes over all projects (both locally and remotely) and updates them as needed
# This essentially replicates the process in perform-sync.py, just does it on the request of the remote Gitlab server
def updateAllProjects():
	# Setup our repository manager
	repositoryManager = Replicant.Manager( repositoryManagerConfig )

	# Setup a local list of all the projects we know about on the Gitlab side
	# We'll need this to check if all the locally held repositories still exist
	knownGitlabProjects = {}

	# Start going over all the projects known to the Gitlab server we are connected to
	# Each project is assumed to represent a repository in this instance
	gitlabProjects = gitlabServer.projects.list(as_list=False)
	for gitlabProject in gitlabProjects:
		# Grab the project in question from our local repository manager
		# Trying to register an already registered project is harmless, so we always do a registration
		localProject = repositoryManager.register( gitlabProject.attributes )

		# Make sure the metadata for the local mirror is up to date from Gitlab
		# Registration does not do this for existing projects, hence the need to do it here
		localProject.updateMetadataFromGitlab( gitlabProject.attributes )

		# Make a note that this is a project confirmed to exist on Gitlab for later use
		knownGitlabProjects[ localProject.path ] = localProject

	# Now that we know about all the repositories, it's time to update them!
	for hash, repository in repositoryManager.knownRepositories.items():
		# Has the repository in question been deleted already? (if it has, there is nothing to sync)
		if repository.deleted:
			continue

		# Check to see if the repository no longer exists on Gitlab
		if repository.path not in knownGitlabProjects:
			# As this repository no longer exists on Gitlab, we can assume it has been deleted (and should therefore skip it)
			repositoryManager.markAsDeleted( repository )
			continue

		# We should also check to see if the remote side has changed since we were last updated
		# If it hasn't, then we can skip the refresh
		if repository.remote_last_update <= repository.last_updated_at:
			# Skip it - we will write out the metadata though just in case that has changed!
			repository.save()
			continue

		# For all other repositories, sync them down!
		repository.refreshMirror()

	# Perform an update of the symlink tree
	repositoryManager.updateExportedTree()
	
	# All done!
	return True
