import json
import falcon

# Falcon Resource handler for Gitlab System Hook events
class GitlabSystemHookResource(object):

	# Initial Setup
	def __init__(self, redisQueue, gitlabToken):
		# Store everything we need to operate
		self.redisQueue  = redisQueue
		self.gitlabToken = gitlabToken

	# Receive a submission from Gitlab
	def on_post(self, req, resp):
		# First check to make sure the submission comes from Gitlab
		if req.get_header('X-Gitlab-Token') != self.gitlabToken:
			raise falcon.HTTPForbidden('Your request is not valid.')

		# Now that we know we are dealing with a Gitlab System Hook submission, we can decode it
		# We expect to receive a JSON formatted payload
		try:
			body = req.stream.read(req.content_length or 0)
			print(body)
			systemEvent = json.loads(body.decode('utf-8'))
		except:
			raise falcon.HTTPBadRequest('Invalid Submission', 'Valid JSON documents are required.')

		# Has a project been created, renamed, moved (transferred) or updated (including being pushed to)?
		if 'event_name' in systemEvent and systemEvent['event_name'] in ['project_create', 'project_rename', 'project_transfer', 'project_update', 'repository_update']:
			# Trigger an update for this specific project
			self.redisQueue.enqueue('Replicant.WebEvents.updateProject', systemEvent['project_id'])

		# Has a merge request involving the project been created/updated/etc?
		if 'event_type' in systemEvent and systemEvent['event_type'] == 'merge_request':
			# Trigger an update for the involved project
			self.redisQueue.enqueue('Replicant.WebEvents.updateProject', systemEvent['project']['id'])

		# Has a project been deleted?
		if 'event_name' in systemEvent and systemEvent['event_name'] == 'project_destroy':
			# Trigger the deletion of this project
			self.redisQueue.enqueue('Replicant.WebEvents.deleteProject', systemEvent['project_id'])

		# Has a user or group been renamed or deleted?
		if 'event_name' in systemEvent and systemEvent['event_name'] in ['user_rename', 'user_destroy', 'group_rename', 'group_destroy']:
			# Because this has wide ranging implications for many repositories, we trigger a global sync for this
			self.redisQueue.enqueue('Replicant.WebEvents.updateAllProjects')

		# Prepare our response
		jsonResponse = {'message': 'OK'}

		# Give Gitlab the all clear that we've done what is needed
		resp.status = falcon.HTTP_200
		resp.body = json.dumps(jsonResponse)
