#!/usr/bin/python3
import os
import sys
import gitlab
import argparse
import Replicant

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Utility to manually perform a full sync of the mirrored repositories')
parser.add_argument('--config', help='Path to the configuration file to work with', required=True)
args = parser.parse_args()

# Make sure our configuration file exists
if not os.path.exists( args.config ):
	print("Unable to locate specified configuration file: %s".format(args.config))
	sys.exit(1)

# Now that we know the file exists, initialize our manager
repositoryManager = Replicant.Manager( args.config )

# Connect to the upstream Gitlab server we will be working with
# To do this, we need to get our credentials and hostname first
gitlabHost  = repositoryManager.config.get('Gitlab', 'instance')
gitlabToken = repositoryManager.config.get('Gitlab', 'token')

# Now we can actually connect
gitlabServer = gitlab.Gitlab( gitlabHost, private_token=gitlabToken )

# Setup a local list of all the projects we know about on the Gitlab side
# We'll need this to check if all the locally held repositories still exist
knownGitlabProjects = {}

# Start going over all the projects known to the Gitlab server we are connected to
# Each project is assumed to represent a repository in this instance
gitlabProjects = gitlabServer.projects.list(as_list=False)
for gitlabProject in gitlabProjects:
	# Grab the project in question from our local repository manager
	# Trying to register an already registered project is harmless, so we always do a registration
	localProject = repositoryManager.register( gitlabProject.attributes )

	# Make sure the metadata for the local mirror is up to date from Gitlab
	# Registration does not do this for existing projects, hence the need to do it here
	localProject.updateMetadataFromGitlab( gitlabProject.attributes )

	# Make a note that this is a project confirmed to exist on Gitlab for later use
	knownGitlabProjects[ localProject.path ] = localProject

	# Note the name of this project to the user
	print("= Registered " + gitlabProject.path_with_namespace)

# Now that we know about all the repositories, it's time to update them!
for hash, repository in repositoryManager.knownRepositories.items():
	# Has the repository in question been deleted already? (if it has, there is nothing to sync)
	if repository.deleted:
		continue

	# Check to see if the repository no longer exists on Gitlab
	if repository.path not in knownGitlabProjects:
		# As this repository no longer exists on Gitlab, we can assume it has been deleted (and should therefore skip it)
		print("Deleting: " + repository.path)
		repositoryManager.markAsDeleted( repository )
		continue

	# We should also check to see if the remote side has changed since we were last updated
	# If it hasn't, then we can skip the refresh
	if repository.remote_last_update <= repository.last_updated_at:
		# Skip it - we will write out the metadata though just in case that has changed!
		print("Skipping: " + repository.path)
		repository.save()
		continue

	# For all other repositories, sync them down!
	print("Syncing:  " + repository.path)
	repository.refreshMirror()

# Perform an update of the symlink tree
repositoryManager.updateExportedTree()

# All done!
sys.exit(0)
