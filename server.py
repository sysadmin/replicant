#!/usr/bin/env python
import os
import rq
import sys
import redis
import falcon
import argparse
import Replicant
import configparser
import wsgiref.simple_server

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Webservice worker to receive notifications from Gitlab and queue them for processing')
parser.add_argument('--config', help='Path to the configuration file to work with', required=True)
args = parser.parse_args()

# Make sure our configuration file exists
if not os.path.exists( args.config ):
	print("Unable to locate specified configuration file: %s".format(args.config))
	sys.exit(1)

# Read in our configuration
configuration = configparser.ConfigParser( interpolation=configparser.ExtendedInterpolation() )
configuration.read( args.config, encoding='utf-8' )

# Connect to the Redis Server
redisSocket = configuration.get('Webservice', 'redis-socket')
redisServer = redis.Redis( unix_socket_path=redisSocket )

# Now bring the Redis Queue online
queueName   = configuration.get('Webservice', 'queue-name')
redisQueue  = rq.Queue( queueName, connection=redisServer )

# Setup the main application
app = falcon.API( middleware=[
])

# Setup the falcon controllers
gitlabValidationToken = configuration.get('Webservice', 'gitlab-token')
gitlabSystemHooks     = Replicant.GitlabSystemHookResource( redisQueue, gitlabValidationToken )

# Add our various routes
app.add_route('/gitlab/system-hooks', gitlabSystemHooks)

# Useful for debugging problems in your API; works with pdb.set_trace(). You
# can also use Gunicorn to host your app. Gunicorn can be configured to
# auto-restart workers when it detects a code change, and it also works
# with pdb.
if __name__ == '__main__':
	# Read in the appropriate configuration
	listenOnHost = configuration.get('Webservice', 'server-host')
	listenOnPort = configuration.getint('Webservice', 'server-port')

	# Setup the webserver...
	httpd = wsgiref.simple_server.make_server( listenOnHost, listenOnPort, app)
	httpd.serve_forever()
